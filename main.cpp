/*
 * LightControl.cpp
 *
 * Created: 03.07.2018 23:17:31
 * Author : Yaroslav
 */ 

#define F_CPU 1200000UL

#include <avr/io.h>
#include <util/delay.h>

#define LEDPin _BV(PB4)
#define RelayPin _BV(PB0)
#define ButtonPin _BV(PB2)



void setLight(bool _p){
	if(_p){
		PORTB |= LEDPin;
		PORTB |= RelayPin;
	}
	else{
		PORTB &= ~LEDPin;
		PORTB &= ~RelayPin;
	}
}

void adc_setup (void)
{
	// Set the ADC input to PB3/ADC3
	ADMUX |= (1 << MUX0);
	ADMUX |= (1 << MUX1);
	ADMUX |= (1 << ADLAR);
	
	// Set the prescaler to clock/128 & enable ADC
	// At 9.6 MHz this is 75 kHz.
	// See ATtiny13 datasheet, Table 14.4.
	ADCSRA |= (1 << ADPS1) | (1 << ADPS0) | (1 << ADEN);
}

int adc_read (void)
{
	// Start the conversion
	ADCSRA |= (1 << ADSC);
	
	// Wait for it to finish
	while (ADCSRA & (1 << ADSC));
	
	return ADCH;
}

void pwm_setup (void)
{
	// Set Timer 0 prescaler to clock/8.
	// At 9.6 MHz this is 1.2 MHz.
	// See ATtiny13 datasheet, Table 11.9.
	TCCR0B |= (1 << CS01);
	
	// Set to 'Fast PWM' mode
	TCCR0A |= (1 << WGM01) | (1 << WGM00);
	
	// Set the prescaler to clock/128 & enable ADC
	ADCSRA |= (1 << ADPS1) | (1 << ADPS0) | (1 << ADEN);
	
	// Clear OC0B output on compare match, upwards counting.
	TCCR0A |= (1 << COM0B1);
}

void pwm_write (int val)
{
	OCR0B = val;
}

uint8_t analogRead(){
	uint8_t adc_in = 0;
	// Get the ADC value
	adc_in = adc_read();
	// Now write it to the PWM counter
	pwm_write(adc_in);
	return adc_in;
}

bool readButton(){
	return (PINB & (1 << PINB2));
}

int main (void)
{
	// LED is an output.
	DDRB |= LEDPin;
	DDRB |= RelayPin;
	// input.
	DDRB &= ~ButtonPin;
	

	adc_setup();
	pwm_setup();
	
	// ------------------
	uint8_t adc_in = 0;
	uint8_t	thresholdValue = 20;
	uint8_t thresholdPercent = 10;
	
	// main loop
	while (1) {
		adc_in = analogRead();
			if (readButton())
			{
				thresholdValue = adc_in;
			}
			if (adc_in <= thresholdValue)
			{
				setLight(true);
			} 
			if (adc_in >= thresholdValue + thresholdPercent)
			{
				setLight(false);
			}
	}
}

